from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import redirect


from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth import login as django_login
from django.contrib.auth import logout as django_logout
from . import models

def index_view(request):

    if request.user.is_authenticated:
        return render(request, 'index.html', {'nbar':'map'})
    else:
        return render(request, 'login.html')

def auth(request):
    user = authenticate(username=request.POST.get('username',''), password=request.POST.get('password',''))
    try:
        django_login(request, user)
        return redirect('/')
    except:
        return redirect('/')

def logout(request):
    django_logout(request)
    return redirect('/')

def register_view(request):
    if request.user.is_authenticated:
        return redirect('/')
    else:
        return render(request, 'register.html')

def newuser(request):

    rut = request.POST.get('rut','')
    nombre = request.POST.get('nombre','')
    contrasena = request.POST.get('contrasena','')
    correo = request.POST.get('correo','')
    direccion = request.POST.get('direccion','')

    try:
        user = User.objects.create_user(username = rut, email = correo, password = contrasena, first_name = nombre)
        a = User.objects.get(username = rut)
        a.profile.direccion = direccion
        a.save()
        return HttpResponse('Usuario creado exitosamente')
    except:
        return HttpResponse('No se pudo crear usuario')

def myparkings_view(request):
    if request.user.is_authenticated:
        parkings = models.Parking.objects.filter(dueno = request.user)
        return render(request, 'myparkings.html',{'nbar':'myparkings','parkings':parkings})
    else:
        return redirect('/')

def addparking(request):
    
    direccion = request.POST.get('direccion', '')
    precio = request.POST.get('precio', 1)
    p = models.Parking(direccion = direccion, precio = precio, dueno = request.user)
    p.save()
    return redirect('/myparkings/')

def removeparking(request):

    id = request.POST.get('id')
    p = models.Parking.objects.get(id = id)

    p.delete()
    return redirect('/myparkings/')

def enableparking(request):

    id = request.POST.get('id')
    p = models.Parking.objects.get(id = id)

    p.state = models.AVAILABLE
    p.save()
    return redirect('/myparkings/')

def disableparking(request):

    id = request.POST.get('id')
    p = models.Parking.objects.get(id = id)

    p.state = models.DISABLED
    p.save()
    return redirect('/myparkings/')

def editparking(request):

    id = request.POST.get('id')
    p = models.Parking.objects.get(id = id)

    return render(request, 'editparking.html', {'parking':p,'nbar':'myparkings'})

def saveparking(request):

    direccion = request.POST.get('direccion', '')
    precio = request.POST.get('precio', 1)
    id = request.POST.get('id')
    p = models.Parking.objects.get(id = id)

    p.direccion = direccion
    p.precio = precio
    p.save()

    return redirect('/myparkings/')

def mycars_view(request):
    if request.user.is_authenticated:

        cars = models.Car.objects.filter(dueno = request.user)
        return render(request, 'mycars.html',{'nbar':'mycars','cars':cars})
    else:
        return redirect('/')

def editcar(request):

    id = request.POST.get('id')
    c = models.Car.objects.get(id = id)

    return render(request, 'editcar.html', {'car':c,'nbar':'mycars'})

def removecar(request):

    id = request.POST.get('id')
    c = models.Car.objects.get(id = id)

    c.delete()

    return redirect('/mycars/')

def addcar(request):

    marca = request.POST.get('marca', '')
    modelo = request.POST.get('modelo', '')
    color = request.POST.get('color', '')
    patente = request.POST.get('patente', '')

    car = models.Car(dueno = request.user, marca = marca, modelo = modelo, color = color, patente = patente)
    car.save()

    return redirect('/mycars/')

def savecar(request):

    marca = request.POST.get('marca', '')
    modelo = request.POST.get('modelo', '')
    color = request.POST.get('color', '')
    patente = request.POST.get('patente', '')
    id = request.POST.get('id')

    car = models.Car.objects.get(id = id)
    car.marca = marca
    car.modelo = modelo
    car.color = color
    car.patente = patente
    car.save()

    return redirect('/mycars/')

def profile(request):
    return render(request, 'profile.html', {})

def saveprofile(request):
    id = request.user.id
    name = request.POST.get('name', '')
    email = request.POST.get('email', '')
    direccion = request.POST.get('direccion', '')

    u = User.objects.get(id = id)
    u.first_name = name
    u.email = email
    u.profile.direccion = str(direccion)
    u.save()
    return redirect('/profile/')

def savepw(request):
    id = request.user.id
    newpass = request.POST.get('newpassword')
    u = User.objects.get(id = id)

    u.set_password(newpass)
    u.save()
    django_logout(request)

    return redirect('/')