from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    direccion = models.CharField(max_length=100, null=True, blank=True)
    def __str__(self):
        return self.user.first_name

DISABLED = "Deshabilitado"
AVAILABLE = "Disponible"
TAKEN = "Arrendado"

STATE_CHOICES = (
    (DISABLED, "Deshabilitado"),
    (AVAILABLE, "Disponible"),
    (TAKEN, "Arrendado")
)

class Car(models.Model):
    marca = models.CharField(max_length = 100)
    modelo = models.CharField(max_length = 100)
    color = models.CharField(max_length = 100)
    patente = models.CharField(max_length = 100)
    dueno = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    def __str__(self):
        return self.patente

class Parking(models.Model):
    direccion = models.CharField(max_length=100)
    precio = models.IntegerField()
    dueno = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    state = models.CharField(max_length=100, choices=STATE_CHOICES, default=DISABLED)
    def __str__(self):
        return self.direccion

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()