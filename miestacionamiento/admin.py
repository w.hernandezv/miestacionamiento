from . import models
from django.contrib import admin

admin.site.register(models.Profile)
admin.site.register(models.Car)
admin.site.register(models.Parking)