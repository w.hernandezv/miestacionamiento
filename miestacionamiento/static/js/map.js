//instantiate leaflet map
var map = L.map('mapid').setView([-33.3656145,-70.6785146], 15);

//agregar marcadores
L.marker([-33.3656145,-70.6785146],{icon:RedIcon}).addTo(map)
    .bindPopup('Usted esta aqui');

//agregar circunferencia
var circle = L.circle([-33.3656145,-70.6785146], {
    color: 'Red',
    fillColor: '#f03',
    fillOpacity: 0.3,
    radius: 2000
}).addTo(map);

L.marker([-33.369388, -70.678543]).addTo(map)
    .bindPopup('Costo por hora: $1000<br><br><button>Arrendar</button>')
L.marker([-33.364295, -70.676791]).addTo(map)
    .bindPopup('Costo por hora: $500<br><br><button>Arrendar</button>')
L.marker([-33.359171, -70.680875]).addTo(map)
    .bindPopup('Costo por hora: $450<br><br><button>Arrendar</button>')
L.marker([-33.368885, -70.682807]).addTo(map)
    .bindPopup('Costo por hora: $700<br><br><button>Arrendar</button>')
L.marker([-33.359709, -70.671434]).addTo(map)
    .bindPopup('Costo por hora: $550<br><br><button>Arrendar</button>')
L.marker([-33.359637, -70.678687]).addTo(map)
    .bindPopup('Costo por hora: $300<br><br><button>Arrendar</button>')
L.marker([-33.365515, -70.669416]).addTo(map)
    .bindPopup('Costo por hora: $800<br><br><button>Arrendar</button>')
L.marker([-33.367809, -70.690189]).addTo(map)
    .bindPopup('Costo por hora: $600<br><br><button>Arrendar</button>')
L.marker([-33.356769, -70.690960]).addTo(map)
    .bindPopup('Costo por hora: $500<br><br><button>Arrendar</button>')
L.marker([-33.360246, -70.687613]).addTo(map)
    .bindPopup('Costo por hora: $650<br><br><button>Arrendar</button>')
L.marker([-33.373688, -70.672463]).addTo(map)
    .bindPopup('Costo por hora: $500<br><br><button>Arrendar</button>')
L.marker([-33.352182, -70.674524]).addTo(map)
    .bindPopup('Costo por hora: $1000<br><br><button>Arrendar</button>')
L.marker([-33.351465, -70.681175]).addTo(map)
    .bindPopup('Costo por hora: $850<br><br><button>Arrendar</button>')
L.marker([-33.377235, -70.688215]).addTo(map)
    .bindPopup('Costo por hora: $500<br><br><button>Arrendar</button>')
L.marker([-33.377128, -70.678515]).addTo(map)
    .bindPopup('Costo por hora: $750<br><br><button>Arrendar</button>')

//agregar basemap desde carto.com
L.tileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, &copy; <a href="https://carto.com/attributions">CARTO</a>'
}).addTo(map);