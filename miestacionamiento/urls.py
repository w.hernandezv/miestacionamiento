from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index_view, name='index_view'),
    path('auth/', views.auth, name='auth'),
    path('logout/', views.logout, name='logout'),
    path('register/', views.register_view, name='register_view'),
    path('register/newuser/', views.newuser, name='newuser'),
    path('myparkings/', views.myparkings_view, name='myparkings_view'),
    path('myparkings/add/', views.addparking, name='addparking'),
    path('myparkings/remove/', views.removeparking, name='removeparking'),
    path('myparkings/enable/', views.enableparking, name='enableparking'),
    path('myparkings/disable/', views.disableparking, name='disableparking'),
    path('myparkings/edit/', views.editparking, name='editparking'),
    path('myparkings/edit/save/', views.saveparking, name='saveparking'),
    path('mycars/', views.mycars_view, name='mycars_view'),
    path('mycars/edit/', views.editcar, name='editcar'),
    path('mycars/remove/', views.removecar, name='removecar'),
    path('mycars/add/', views.addcar, name='addcar'),
    path('mycars/edit/save/', views.savecar, name='savecar'),
    path('profile/', views.profile, name='profile'),
    path('profile/save/', views.saveprofile, name='saveprofile'),
    path('profile/savepw/', views.savepw, name='savepw')

]
